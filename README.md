SEWA Unity Client

For CommandlineClient

Add this as a submodule to your consuming project.
Copy the tools folder from sewa-unity to the root of your project.

Your project structure should look like:

Project Root 
 | - sewa-unity
 | - tools
     | - create_symlinks.bat
 | - unity
     | - Assets
     | - Project Settings
... etc

Once setup, double click create_symlinks.bat


For WebClient

Note: This requires the sewa server being available at http://sewa-image:8080

To host the sewa-image locally:
1. Modify your hosts file to resolve "sewa-image" to localhost (127.0.0.1):
1.1. Open: 
C:\Windows\System32\drivers\etc\hosts

1.2. Add: 
127.0.0.1 sewa-image

2. Run the sewa-image:
2.1. Get the source from:
https://gitlab.com/playgen/sewa-image

2.2. Open the ./tools folder and run:
build_and_run_sewa-image.bat


To use the version hosted on librarian:
This uses nginx as a proxy to redirect to the version hosted on librarian
1. Modify your hosts file to resolve "sewa-image" to localhost.

2. Add to your nginx.conf
# sewa-image 
server {
    listen       127.0.0.1:8080;
    server_name  sewa-image;
    location / {
        proxy_bind [your static ip address here];
        proxy_pass http://10.0.0.2:45808$uri$is_args$query_string;
        proxy_set_header Host localhost;
        proxy_pass_request_headers on;
        proxy_pass_request_body on;
   }
}

3. Add your static ip address e.g:
proxy_bind [your static ip address here];

becomes

proxy_bind 10.0.0.14;

4. Restart your nginx service