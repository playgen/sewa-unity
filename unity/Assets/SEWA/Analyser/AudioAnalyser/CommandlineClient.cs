﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace PlayGen.Assets.SEWA.Analyser.AudioAnalyser
{
	/// <summary>
	/// This will currentl only work in the Unity Editor on Windows
	/// </summary>
	public class CommandlineClient : IAudioAnalyser
	{
		public CommandlineClient(MonoBehaviour monoBehaviourContext)
		{
			_monoBehaviourContext = monoBehaviourContext;
		}

		public static readonly Dictionary<string, int> Features = new Dictionary<string, int>
		{
			// 2 Classes
			{"Anger", 2},
			{"Conflict", 2},
			{"Deception", 2},
			{"Likability", 2},
			{"Sadness", 2},
			// 3 Classes
			{"Positivity", 3},
			{"Boredom", 3},
			{"Happiness", 3},
			{"Interest", 3},
			{"Sincerity", 3},
			{"Sleepiness", 3},
			{"Stress", 3},
			{"Volume", 3}
		};

		private readonly MonoBehaviour _monoBehaviourContext;

		public void Dispose()
		{
			throw new NotImplementedException();
		}

		public void Analyse(byte[] data, string audioFilePath, AnalysisCompletedHandler analysisCompletedHandler)
		{
			_monoBehaviourContext.StartCoroutine(AnalysisRoutine(data, audioFilePath, analysisCompletedHandler));
		}

		public IEnumerator AnalysisRoutine(byte[] data, string audioPath, AnalysisCompletedHandler analysisCompletedHandler)
		{
			var fullAnalysisDir = Path.GetFullPath(Application.streamingAssetsPath) + "\\audio_analysis";

			var analysisGuid = Guid.NewGuid();
			var analysisTempDir = fullAnalysisDir + "\\temp\\" + analysisGuid;
			Directory.CreateDirectory(analysisTempDir);

			var tempAudioPath = analysisTempDir + "\\" + Path.GetFileName(audioPath);
			Debug.Log("Saving to: " + tempAudioPath);
			File.WriteAllBytes(tempAudioPath, data);

			var lldPath = analysisTempDir + "\\LLD.csv";

			// SMILE EXTRACT
			var smilExtractInfo = new ProcessStartInfo()
			{
				WorkingDirectory = fullAnalysisDir,
				FileName = "SMILExtract.exe",
				Arguments = "-C SMILE.ComParE.LLD.conf -logfile smile.log -I \"" + tempAudioPath + "\" -instname \"" + tempAudioPath + "\" -lldcsvoutput \"" + lldPath + "\" - l 1",
				WindowStyle = ProcessWindowStyle.Hidden
			};

			var smileExtractProcess = Process.Start(smilExtractInfo);

			while (!smileExtractProcess.HasExited)
			{
				yield return new WaitForSeconds(0.25f);
			}

			var analysisResult = new AnalysisResult()
			{
				Results = new Dictionary<string, float>(),
				VerboseResults = new Dictionary<string, FeatureResult>()
			};

			var processes = new List<Process>();
			
			foreach (var feature in Features.Keys)
			{
				var featureInfo = new ProcessStartInfo()
				{
					WorkingDirectory = fullAnalysisDir,
					FileName = "java",
					Arguments = "-jar openXBOW.jar -i \"" + lldPath + "\" -svmModel models/" + feature +
								".model -oJson \"" + analysisTempDir + "\\" + feature + ".json\" -attributes nt1[65]2[65] -norm 1 -b models/" + feature +
								".codebook",
					CreateNoWindow = true,
					UseShellExecute = false,
					RedirectStandardError = true
				};

				var featureProcess = Process.Start(featureInfo);
				processes.Add(featureProcess);
			}

			while (processes.Any(p => !p.HasExited))
			{
				yield return new WaitForSeconds(0.25f);
			}

			processes.ForEach(p =>
			{
				var error = p.StandardError.ReadToEnd();
				if (!string.IsNullOrEmpty(error))
				{
					Debug.LogError(error);
				}

			});

			foreach (var feature in Features.Keys)
			{
				var resultString = File.ReadAllText(analysisTempDir + "\\" + feature + ".json");
				var result = JsonConvert.DeserializeObject<SerializedFeatureResult>(resultString);
				analysisResult.Results[feature] = result.PredictionScaled;

				analysisResult.VerboseResults[feature] = result.ToFeatureResult();
			}

			analysisCompletedHandler(analysisResult);

			Directory.Delete(analysisTempDir, true);
		}
	}

	public class FeatureResult
	{
		public ClassifierTypes ClassifierType { get; set; }

		/// <summary>
		/// 0 - 1 with 1 being a high prediction for the Classifier Type
		/// </summary>
		public float ClassifierValue { get; set; }

		/// <summary>
		/// Scaled from -1 to 1 with 0 as neutral
		/// </summary>
		public float PredictionScaled { get; set; }

		public int ClassifierPrediction { get; set; }
	}

	public enum ClassifierTypes
	{
		Negative,
		Neutral,
		Positive
	}

	public class SerializedFeatureResult
	{
		public string Index { get; set; }

		public string Feature { get; set; }

		public float Class1Prob { get; set; }

		public float Class2Prob { get; set; }

		public float Class3Prob { get; set; }

		public int Prediction { get; set; }

		public FeatureResult ToFeatureResult()
		{
			return new FeatureResult()
			{
				ClassifierPrediction = Prediction,
				ClassifierType = ClassifierType,
				ClassifierValue = ClassifierValue,
				PredictionScaled = PredictionScaled,
			};
		}

		public ClassifierTypes ClassifierType
		{
			get
			{
				var classifier = Prediction - 1;

				var classifiers = CommandlineClient.Features[Feature];
				if (classifiers == 2)
				{
					classifier += 1;
				}

				return ClassifierTypeMappings[classifier];
			}
		}

		public float ClassifierValue
		{
			get
			{
				switch (Prediction)
				{
					case 1:
						return Class1Prob;
					case 2:
						return Class2Prob;
					case 3:
						return Class3Prob;
					default:
						throw new Exception();
				}
			}
		}

		public float PredictionScaled
		{
			get
			{
				switch (ClassifierType)
				{
					case ClassifierTypes.Negative:
						return 1 - ClassifierValue;

					case ClassifierTypes.Neutral:
						return 0;

					case ClassifierTypes.Positive:
						return ClassifierValue;

					default:
						throw new ArgumentOutOfRangeException();
				}

			}
		}

		public static ClassifierTypes[] ClassifierTypeMappings =
			{ClassifierTypes.Negative, ClassifierTypes.Neutral, ClassifierTypes.Positive};
	}
}