﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using PlayGen.Assets.SEWA.Analyser.AudioAnalyser;
using PlayGen.Assets.SEWA.Analyser.Tests.AudioAnalyser;
using PlayGen.Assets.SEWA.Tests;
using UnityEngine;
using UnityEngine.TestTools;

namespace PlayGen.Assets.SEWA.Analyser.Tests
{
    public class MicAnalyserTests : PlayModeTests
    {
        [UnityTest]
        public IEnumerator CanAnalyseMic_WebClient_1_5000_1000()
        {
            return CanAnalyseMic_WebClient(1, 5000, 1000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_WebClient_10_5000_1000()
        {
            return CanAnalyseMic_WebClient(10, 5000, 1000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_WebClient_1_10000_1000()
        {
            return CanAnalyseMic_WebClient(1, 10000, 1000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_WebClient_10_10000_1000()
        {
            return CanAnalyseMic_WebClient(10, 10000, 1000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_CommandineClient_1_5000_1000()
        {
            return CanAnalyseMic_CommandlineClient(1, 5000, 1000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_CommandineClient_10_5000_1000()
        {
            return CanAnalyseMic_CommandlineClient(10, 5000, 1000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_CommandineClient_1_10000_1000()
        {
            return CanAnalyseMic_CommandlineClient(1, 10000, 1000);
        }

        [UnityTest]
        public IEnumerator CanAnalyseMic_CommandineClient_10_10000_1000()
        {
            return CanAnalyseMic_CommandlineClient(10, 10000, 1000);
        }

        public IEnumerator CanAnalyseMic_WebClient(int itterations, int segmentLength, int analysisInterval)
        {
            var audioAnalyser = new WebClient(WebClientTests.URL, MonoBehaviourContext);
            return CanAnalyseMic(itterations, segmentLength, analysisInterval, audioAnalyser);
        }

        public IEnumerator CanAnalyseMic_CommandlineClient(int itterations, int segmentLength, int analysisInterval)
        {
            var audioAnalyser = new CommandlineClient(MonoBehaviourContext);
            return CanAnalyseMic(itterations, segmentLength, analysisInterval, audioAnalyser);
        }

        public IEnumerator CanAnalyseMic(int itterations, int segmentLength, int analysisInterval, IAudioAnalyser audioAnalyser)
        {
            // Arrange
            var leeway = 10 * 1000;
            var analysisResults = new List<AnalysisResult>();
            var micAnalyzer = new MicAnalyser(segmentLength, analysisInterval, MonoBehaviourContext, audioAnalyser, analysisResults.Add);

            // Act
            micAnalyzer.StartAnalysis();

            yield return new WaitForSeconds((itterations * segmentLength + leeway) / 1000f);

            micAnalyzer.StopAnalysis();

            // Assert
            Assert.GreaterOrEqual(analysisResults.Count, itterations);
            analysisResults.ForEach(Assert.NotNull);
        }
    }
}
