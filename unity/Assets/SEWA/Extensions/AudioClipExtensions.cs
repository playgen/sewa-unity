﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioClipExtensions
{
    public static AudioClip CreateSegment(this AudioClip source, int sampleLength, int offset)
    {
        var segment = AudioClip.Create(source.name, sampleLength, source.channels, source.frequency, false);
        
        var samples = new float[sampleLength];
        source.GetData(samples, offset);
        segment.SetData(samples, 0);

        return segment;
    }
}
