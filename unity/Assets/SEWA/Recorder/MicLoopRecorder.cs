﻿using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace PlayGen.Assets.SEWA.Recorder
{
    /// <summary>
    ///  Must be run on the main thread as dictated by the UnityEngine Microphone api.
    /// </summary>
    public class MicLoopRecorder : IDisposable
    {
        private readonly int _segmentLength;
        private readonly int _segmentInterval;
        private readonly MicRecorder _micRecorder;
        private readonly MonoBehaviour _monoBehaviourContext;
        private readonly RecordingCompletedHandler _recordingCompletedHandler;

        private bool _isDisposed;
        private IEnumerator _recordCoroutine;

        public delegate void RecordingCompletedHandler(AudioClip recording);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="segmentLength">Length of each recording segment.</param>
        /// <param name="segmentInterval">Interval at which new segments will be created.</param>
        /// <param name="monoBehaviourContext"></param>
        /// <param name="recordingCompletedHandler">Callback for when new segments have been created.</param>
        public MicLoopRecorder(int segmentLength, int segmentInterval, MonoBehaviour monoBehaviourContext, RecordingCompletedHandler recordingCompletedHandler)
        {
            _segmentLength = segmentLength;
            _segmentInterval = segmentInterval;
            _monoBehaviourContext = monoBehaviourContext;
            _recordingCompletedHandler = recordingCompletedHandler;
            _micRecorder = new MicRecorder();
        }

        public void Start()
        {
            if (_recordCoroutine != null)
            {
                throw new InvalidOperationException("This instance is already recording. Make sure to stop first or create a new instance.");    
            }

            _micRecorder.Start();
            _recordCoroutine = RecordCoroutine(_micRecorder.CurrentClip);
            _monoBehaviourContext.StartCoroutine(_recordCoroutine);
        }

        public void Stop()
        {
            if (_recordCoroutine != null)
            {
                _monoBehaviourContext.StopCoroutine(_recordCoroutine);
                _recordCoroutine = null;
            }
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            Stop();

            _isDisposed = true;
        }

        private IEnumerator RecordCoroutine(AudioClip micRecording)
        {
            var stopWatch = Stopwatch.StartNew();
            var previousSampleEnd = 0;
            var recordingLengthSamples = Mathf.CeilToInt(micRecording.frequency * (_segmentLength / 1000f));
            var sampleEndIncrement = Mathf.CeilToInt(micRecording.frequency * (_segmentInterval / 1000f));

            while (true)
            {
                var nextSampleEnd = previousSampleEnd + recordingLengthSamples;
                var elapsedSamples = Mathf.FloorToInt((stopWatch.ElapsedMilliseconds / 1000f) * micRecording.frequency);
                if (elapsedSamples > nextSampleEnd)
                {
                    var recording = micRecording.CreateSegment(recordingLengthSamples, nextSampleEnd - recordingLengthSamples);
                    previousSampleEnd += sampleEndIncrement;

                    _recordingCompletedHandler(recording);
                }

                yield return null;
            }
        }
    }
}
