﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PlayGen.Assets.SEWA.Recorder
{
    /// <summary>
    ///  Must be run on the main thread as dictated by the UnityEngine Microphone api.
    /// </summary>
    public class MicRecorder : IDisposable
    {
        private readonly int _maxLengthSeconds;
        private readonly string _deviceName;
        private readonly int _frequency;
        
        private bool _isDisposed;

        public AudioClip CurrentClip { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceName">Default value uses defaulkt recording device</param>
        /// <param name="frequency">Default value uses frequency setting of 44100 Hz</param>
        /// <param name="maxLength">Start length in Milliseconds</param>
        public MicRecorder(string deviceName = "", int frequency = 44100, int maxLength = int.MaxValue)
        {
            _maxLengthSeconds = Mathf.CeilToInt(maxLength / 1000f);
            _deviceName = deviceName;
            _frequency = frequency;
        }

        public void Stop()
        {
            if (Microphone.IsRecording(_deviceName))
            {
                Microphone.End(_deviceName);
            }
        }

        public AudioClip Start()
        {
            CurrentClip = Microphone.Start(_deviceName, false, _maxLengthSeconds, _frequency);
            return CurrentClip;
        }
      
        public void Dispose()
        {
            if (_isDisposed) return;

            Stop();

            _isDisposed = true;
        }
    }
}
