﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using PlayGen.Assets.SEWA.Tests;
using UnityEngine;
using UnityEngine.TestTools;

namespace PlayGen.Assets.SEWA.Recorder.Tests
{
    public class MicLoopRecorderTests : PlayModeTests
    {
        [UnityTest]
        public IEnumerator CanRecord_1_Clips_OfLength_5000_1000()
        {
            return CanRecordClipsCoroutine(1, 5000, 1000);
        }

        [UnityTest]
        public IEnumerator CanRecord_10_Clips_OfLength_5000_1000()
        {
            return CanRecordClipsCoroutine(10, 5000, 1000);
        }

        [UnityTest]
        public IEnumerator CanRecord_1_Clips_OfLength_10000_1000()
        {
            return CanRecordClipsCoroutine(1, 10000, 1000);
        }

        [UnityTest]
        public IEnumerator CanRecord_10_Clips_OfLength_10000_1000()
        {
            return CanRecordClipsCoroutine(10, 10000, 1000);
        }

        public IEnumerator CanRecordClipsCoroutine(int itterations, int segmentLength, int segmentInterval)
        {
            // Arrange
            var segmentLengthLeeway = 1 * 1000;
            var waitLeeway = 1 * 1000;
            var recordings = new List<AudioClip>();

            var micLoopRecorder = new MicLoopRecorder(segmentLength, segmentInterval, MonoBehaviourContext, recording => recordings.Add(recording));

            // Act
            micLoopRecorder.Start();

            var duration = (segmentLength - segmentInterval) + (itterations * segmentInterval);
            var wait = duration + waitLeeway;
            yield return new WaitForSeconds(wait / 1000f);

            micLoopRecorder.Stop();

            // Assert
            Assert.GreaterOrEqual(recordings.Count, itterations);

            foreach (var recording in recordings)
            {
                var clipLength = recording.length * 1000;
                var difference = Mathf.Abs(clipLength - segmentLength);

                Assert.LessOrEqual(difference, segmentLengthLeeway);
            }
        }
    }
}
