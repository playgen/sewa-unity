﻿using System;
using System.IO;
using UnityEngine;

namespace PlayGen.Assets.SEWA.Tests
{
    public abstract class PlayModeTests
    {
        private TestMonoBehaviourContext _monobehaviourContext;

        public static string AudioFilePath
        {
            get
            {
                var type = typeof(PlayModeTests);
                var assembly = type.Assembly;
                var uriBuilder = new UriBuilder(assembly.CodeBase);
                var rootDir = Path.GetDirectoryName(
                    Path.GetDirectoryName(
                        Path.GetDirectoryName(
                            Path.GetDirectoryName(
                                Uri.UnescapeDataString(uriBuilder.Path)))));

                return rootDir + "/test_data/test_short_audio_only.wav";
            }
        }

        protected MonoBehaviour MonoBehaviourContext
        {
            get
            {
                if (_monobehaviourContext == null)
                {
                    var gameObject = new GameObject();
                    _monobehaviourContext = gameObject.AddComponent<TestMonoBehaviourContext>();
                }

                return _monobehaviourContext;
            }
        }
    }
}